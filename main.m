clear all
close all
clc
%% name file
openstreetmap_filename = 'map.mat';
ignoreDirectionOfRoads=1;
exploreParkingLot=1;
simulateUser=1;



%% download map of area using user's android phone for inputs
 [userLocation, goalLocation]=readMapOfArea(simulateUser);

 load('map.mat');  % parsed osm

%% Plot map
fig = figure;
ax = axes('Parent', fig);
ax.XAxis.FontSize = 15;
ax.YAxis.FontSize = 15;
hold(ax, 'on')
% plot the network, optionally a raster image can also be provided for the
% map under the vector graphics of the network
plot_way(ax, parsed_osm)
title('OpenStreetMap osm file', 'FontSize', 15);

%% Create graph
nodes = parsed_osm.node;
node_xys = nodes.xy;
[connectivity_matrix, intersection_node_indices] = extract_connectivity(parsed_osm,node_xys);
intersection_nodes = get_unique_node_xy(parsed_osm, intersection_node_indices);
%% Find  start and goal coordinates

startCoords=[userLocation(2), userLocation(1)];
targetCoords=[goalLocation(2), goalLocation(1)];
maximalDistanceFromTarget=100;


% Find intersections of the graph to the goal and start points
distStart=sum(abs(intersection_nodes.xys'-startCoords),2);
distGoal=sum(abs(intersection_nodes.xys'-targetCoords),2);
startNode=intersection_nodes.xys(:,find(distStart == min(distStart)))';
goalNode=intersection_nodes.xys(:,find(distGoal == min(distGoal)))';
distStart=sum(abs(node_xys'-startNode),2);
distGoal=sum(abs(node_xys'-goalNode),2);
startIndex=find(distStart == min(distStart));
goalIndex=find(distGoal == min(distGoal));



[parkingNodeIndices,parkingNodesOrdered] = ...
    extract_parkings(parsed_osm,goalIndex,node_xys,...
    maximalDistanceFromTarget);

%% Plan path between nodes
start = startIndex; % node global index
target =goalIndex;
if(ignoreDirectionOfRoads)
    dg = or(connectivity_matrix, connectivity_matrix.'); % make symmetric
else
    dg=connectivity_matrix;
end
G=digraph(dg);

numOfNodes=size(G.Nodes,1);

%% find edges to be reached
parkingEdgeIndices=[];
for i=1:size(parkingNodesOrdered,2)
    nodesAlongWay_i=parkingNodesOrdered{:,i};
    %extract edges
    for j=1:length(nodesAlongWay_i)-1
        startNodeAlongWay=nodesAlongWay_i(j);
        endNodeAlongWay=nodesAlongWay_i(j+1);
        [~,~,edgeIdx]=shortestpath(G,startNodeAlongWay,endNodeAlongWay);
        parkingEdgeIndices=[parkingEdgeIndices,edgeIdx];
    end
    
end



%% explore parking lot
if(exploreParkingLot)
    %[route] = pathPlanner(G,parkingNodeIndices,start);
    [route,~] = pathPlanner(G,parkingEdgeIndices',start);
 
else
    [route, dist]=shortestpath(G,start,target);
end
%


%% Plot results
close all
fig = figure;
ax = axes('Parent', fig);
ax.XAxis.FontSize = 30;
ax.YAxis.FontSize = 30;
hold(ax, 'on')

% plot the network, optionally a raster image can also be provided for the
% map under the vector graphics of the network
plot_way(ax, parsed_osm)
%plot_way(ax, parsed_osm, map_img_filename) % if you also have a raster image
plot_route(ax, route, parsed_osm,intersection_node_indices,node_xys)
%plot start and finish
hold on
title('OpenStreetMap osm file', 'FontSize', 30);


% plot(node_xys(1,parkingNodeIndices),node_xys(2,parkingNodeIndices),'bo');
 plot(node_xys(1,startIndex),node_xys(2,startIndex),'mo','MarkerSize',20, 'LineWidth',4);
 plot(node_xys(1,goalIndex),node_xys(2,goalIndex),'b*','MarkerSize',20, 'LineWidth',4);
hold off
hold(ax, 'off')


%% debugging
hold on
graphEdges=G.Edges.EndNodes;
startNodes=graphEdges(parkingEdgeIndices,1)';
endNodes=graphEdges(parkingEdgeIndices,2)';
plot(node_xys(1,startNodes),node_xys(2,startNodes),'bo','MarkerSize',20);
plot(node_xys(1,endNodes),node_xys(2,endNodes),'bo','MarkerSize',20);


