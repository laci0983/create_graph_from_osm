function userLocation = getUserLocation()
% This function returns the coordinates of the user, by receiving an udp
% package containing Lat, Lon, and Alt data at port 25000. 
%If no data is received within 5 seconds, the function return NaNs
%@input:
%
%@output: 
%         Lat  - Latitude of the current position of user
%         Lon  - Longitude of the current position of user


udpr=udpport('LocalPort',25000);
flush(udpr);
%setup timer
tic
%read location info
userLocation=[];
while isempty(userLocation) & toc < 5
      userLocation=read(udpr,3,"double");
end

if(isempty(userLocation))

    userLocation=NaN;
end
    
clear udpr
end

