function [c] = geoCode(address)
%This function performs the geocoding of the destination descriptive string
%Nominatim of openstreetmaps used to receive an xml conatining the
%coordinates of the address
%@param:
%         address - string describing the desired destination
%@return:
%         c     - coordinates of the address (Latitude, Longitude)
%                 Size: 1x2 vector


if isempty(address) ||  ~isvector(address)
    error('Invalid address provided, must be a string');
end
%replace white spaces in the address with '+'
address = urlencode(address);

        
SERVER_URL = 'https://nominatim.openstreetmap.org/search';
queryUrl = sprintf('%s?format=xml&q=%s', SERVER_URL, address);
parseFcn = @parseOpenStreetMapXML;


try
    docNode = xmlread(queryUrl);
catch  %#ok<CTCH>
    error('Error, could not reach %s, is it a valid URL?', SERVER_URL);
end
   
c = parseFcn(docNode);
end

% Function to parse the XML response from OpenStreetMap
function [c] = parseOpenStreetMapXML(docNode)
    
    serverResponse = docNode.getElementsByTagName('searchresults').item(0);
    placeTag = serverResponse.getElementsByTagName('place').item(0);
    
    if isempty(placeTag)
        error('OpenStreeMap returned no data for that address');
    end
    
    c(1) = str2double( char( placeTag.getAttribute('lat') ) );
    c(2) = str2double( char( placeTag.getAttribute('lon') ) );
    
end
function elementText = GetElementText(resultNode,elementName)
% GETELEMENTTEXT given a result node and an element name
% returns the text within that node as a Matlab CHAR array
elementText = ...
    char( resultNode.getElementsByTagName(elementName).item(0).getTextContent );
end
