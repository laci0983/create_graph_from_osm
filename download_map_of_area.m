% get goal configuration data

goalGeoCode=readGeoCodeFromUser();
if(isempty(goalGeoCode))
    disp("No geocode data received");
end
[c] = geoCode(goalGeoCode);
goal_Lat=c(1);
goal_Lon=c(2);

%get user location data
[pos_Lat,pos_Lon] = getUserLocation();
if(isemty(goalGeoCode))
  disp("No position data received");
end


fileName="test.osm";
url_first_part="https://www.openstreetmap.org/api/0.6/map?bbox=";
delimeter="%2C";

[latMid, lonMid] = midpointLatLon(pos_Lat, pos_Lon, goal_Lat, goal_Lon);


degOffset=km2deg(1.5*distance(pos_Lat,pos_Lon,goal_Lat,goal_Lon,6371000)/1000);
bbox_left_lon=min(pos_Lon,goal_Lon)-degOffset;
bbox_down_lat=min(pos_Lat,pos_Lat)-degOffset;
bbox_right_lon=max(pos_Lon,goal_Lon)+degOffset;
bbox_up_lat=max(pos_Lat,pos_Lat)+degOffset;

url=strcat(url_first_part,...
      num2str(bbox_left_lon),delimeter,num2str(bbox_down_lat),delimeter,...
                   num2str(bbox_right_lon),delimeter,num2str(bbox_up_lat));

urlwrite(url,fileName);