function new_population=mutate(population,mutrate)
% The function performs low probablity (mutrate) mutation on the pupulation
%@param:
%        population  - initial population
%        mutrate     - probability of mutation
%@return:
%        new_population - population after mutation

Nind=size(population,1);
Nvar=size(population,2);
new_population=population;
if(mutrate>=rand())
    indToMutate=round(1+rand()*(Nind-1));
    index1=round(1+rand()*(Nvar-1));
    index2=round(1+rand()*(Nvar-1));
    tempGene=new_population(indToMutate,index1);
    new_population(indToMutate,index1)=new_population(indToMutate,index2);
    new_population(indToMutate,index2)=tempGene;
end

