function [parkingIndices,parkinNodesOrdered] = extract_parkings(parsed_osm,...
                                    goalIndex,node_xys,maximalDistance)
%This function provides the indices of the edges along which possible 
%parking places can be found.

[~, node, way, ~] = assign_from_parsed(parsed_osm);
road_vals = {'motorway', 'motorway_link', 'trunk', 'trunk_link',...
    'primary', 'primary_link', 'secondary', 'secondary_link',...
    'tertiary', 'road', 'residential', 'living_street',...
    'service', 'services', 'motorway_junction',...
    'unclassified','parking'};

road_keys={'highway'};
%road_keys={'highway','amenity'};

goalCoords = node_xys(:,goalIndex)';

numOfWays=size(way.id,2);
nodesAlongWays=way.nd;
nodes=node.id;
parkingIndices=[];
parkinNodesOrdered=[];




for i=1:numOfWays
    % Process only highways
    [key, val] = get_way_tag_key(way.tag{1, i} );
    [key,val,isParkingAllowed]=is_parking_allowed(key,val);
    if sum(ismember(road_keys, key) == 1) == 0 |...
            isParkingAllowed~=1
        continue;
    end
    
    % Are vehicles allowed in this type of road?
    if sum(ismember(road_vals, val) == 1) == 0
        continue; % skip if it's a non drivable road
    end
    
    nodesAlongWay_i=nodesAlongWays{i}; % get current driveable way
    nodeIndicesAlongWay=find(ismember(nodes,nodesAlongWay_i))';
    
    way_xys=node_xys(:,nodeIndicesAlongWay)';
    dist=distance(goalCoords(2),goalCoords(1),...
                   way_xys(:,2),way_xys(:,1),6371000);
    if(sum(dist<=maximalDistance)== 0) %if parking is too far
        continue;
    end
    parkinNodesOrdered{end+1}=nodeIndicesAlongWay;
    
    parkingIndices=[parkingIndices;nodeIndicesAlongWay];
    %numOfNodesAlongWay_i=size(nodesAlongWay_i,2); %get all nodes of way
    
end
end

