function selected=select(fvals,ggap)
%Funciton performs SUS - selection
% Function performs selection based on fitness values of the individuals.
%@param:
%       fvals -  fitness values of the population (vector) size: 1 x Nind
%       ggap  -  ration of the selected individuals, function will select
%                N=floor(ggap*Nind) individuals for selection
%@output:
%       selected - number of the selected individuals (vector) size: 1xN

Nind=length(fvals);
N=floor(ggap*Nind);

Sum=sum(fvals);

iterator=Sum/N;
Intervalls=cumsum(fvals);

ptr=rand()*iterator;

i=0:N-1;
ptr_array=ptr+i*iterator;


compareMtrx=ptr_array<Intervalls;

[~,selected]=max(compareMtrx,[],1);




end

