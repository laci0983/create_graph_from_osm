function [path,nodesInOrderOfParkingLot] = pathPlanner(dg,parkingEdgeIndices,startNode)
%This function performs the planning of the parking lot exploration, where
%the graph of the parking lot and the edges of the parking lot that are to
%be reached are available. The exploration start and end on the same
%vertex.
%The planning of the traversal takes the user location into consideration
%thus the initial point of the exploration is selected so that the distance
%between the user and the parking lot exploration start is minimal.
%@param:
%           dg   - directed graph representing the parking lot
%           parkingEdgeIndices - indices of the edges in dg that represent
%                                road sections along which possible parking
%                                places can be found
%           startNode         - index of the initial vertex (user location)
%@return:
%           path             - sequence of vertices of the traversal
%           


global startPoint;
global genNum;
global trendOfError;
global toggle;
global repetable;
repetable=1;
toggle=1;
trendOfError=[];


startPoint=startNode;

nodesInOrderOfParkingLot=[];
genNum=1; %generation counter

Nind=10;
maxgen=100;
ggap=0.8;
mutpr=0.01;
rrate=0.4;


global G;
G=dg;
graphEdges=G.Edges.EndNodes;

numOfNodes=size(G.Nodes,1);

Nvar=size(parkingEdgeIndices,1);


initpop=[];

for i=1:Nind
    idx=randperm(Nvar);
    initpop(end+1,:)=parkingEdgeIndices(idx);
end

disp('Path planning with Genetic algorithm');
% execute the genetic algorithm
fpop=mysga(initpop,maxgen,'objfn',ggap,mutpr,rrate);


%get the best individual after maxgen generations
objvals=objfn(fpop);
[~,I]=min(objvals);

bestPop=fpop(I,:);
edgesInOrder=bestPop;
startNodes=graphEdges(edgesInOrder,1)';
endNodes=graphEdges(edgesInOrder,2)';
% create node to reach in sequence    
% in steps
N = max(numel(startNodes),numel(endNodes));
ia = 1+rem(0:N-1, numel(startNodes)) ;
ib = 1+rem(0:N-1, numel(endNodes));
C = [startNodes(ia) ; endNodes(ib)];
C = reshape(C,1,[]) ;
% in a one-liner
bestRoute = reshape([startNodes(1+rem(0:max(numel(startNodes),numel(endNodes))-1, numel(startNodes))) ;...
    endNodes(1+rem(0:max(numel(startNodes),numel(endNodes))-1, numel(endNodes)))],1,[]);
path=[];
start=startPoint;
for i=1:size(bestRoute,2)
    endPoint=bestRoute(i);
    [nodesInOrder,~]=shortestpath(G,start,endPoint);
    if(i>1)
        nodesInOrderOfParkingLot=[nodesInOrderOfParkingLot, nodesInOrder];
    end
    path=[path, nodesInOrder];
    start=nodesInOrder(end);    
end

if(repetable) 
    [nodesInOrder,~]=shortestpath(G,path(end),bestRoute(:,1));
    path=[path, nodesInOrder];
end

end
