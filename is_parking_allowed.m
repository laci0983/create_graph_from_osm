function [key,val,isparkingAllowed]=is_parking_allowed(key,val)

isparkingAllowed=0;
if(~isempty(val))
    Index = find(contains(val,'parking_aisle'));
    
    if(~isempty(Index))
        isparkingAllowed=1;
    end
    
    if(iscell(val) & isparkingAllowed)
        key=key{1};
        val=val{1};
    end
end

if(isparkingAllowed == 0)
    Index= find(contains(val,'parking'));
    
    if(~isempty(Index))
        isparkingAllowed=1;
        if(iscell(val))
            key=key{Index};
            val=val{Index};
        end
    end  
    
end

if(iscell(val))
    key=key{1};
    val=val{1};
end

end