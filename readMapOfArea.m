function [userLocation, goalLocation]=readMapOfArea(simulateUser)
%This function reads the coordinates of user location and the goal location
%The function uses receives udp packets that are sent from the user's
%adndroid phone vie the sensor_stream app (see sensor_stream.slx)
%@param:
%        simulateUser  - binary input, when :=1 sample coordinates are
%                        returned
%@return:
%        userLocation - coordinates of the user's position received as an
%                       udp packet in port 25000
%                       Size: 1x2 vector
%        goalLocation - coordinates of the desired destination, where the
%                       destination is received as a descriptive string,
%                       via udp at port 25002, and by using geocoding 
%                       the coordinates are extracted.
%                       Size: 1x2 vector
if(simulateUser)
    userLocation = [ 47.4499   19.0391];
    
    goalLocation = [ 47.4470   19.0366];
    return;
end
warning('off')
% get goal configuration data

goalGeoCode=readGeoCodeFromUser();
if(isempty(goalGeoCode))
    error("No geocode data received");
end
[goalLocation] = geoCode(goalGeoCode);
goal_Lat=goalLocation(1);
goal_Lon=goalLocation(2);

%get user location data
userLocation = getUserLocation();
if(isempty(userLocation))
    error("No position data received");
end
pos_Lat=userLocation(1);
pos_Lon=userLocation(2);
userLocation=[pos_Lat,pos_Lon];


fileName='map.osm';
url_first_part="https://www.openstreetmap.org/api/0.6/map?bbox=";
delimeter="%2C";

[latMid, lonMid] = midpointLatLon(pos_Lat, pos_Lon, goal_Lat, goal_Lon);


degOffset=km2deg(distance(pos_Lat,pos_Lon,goal_Lat,goal_Lon,6371000)/1000);
bbox_left_lon=lonMid-degOffset;
bbox_down_lat=latMid-degOffset;
bbox_right_lon=lonMid+degOffset;
bbox_up_lat=latMid+degOffset;

url=strcat(url_first_part,...
    num2str(bbox_left_lon),delimeter,num2str(bbox_down_lat),delimeter,...
    num2str(bbox_right_lon),delimeter,num2str(bbox_up_lat));

try
    urlwrite(url,fileName);
catch
end

%%convert XML -> MATLAB struct
% convert the OpenStreetMap XML Data file donwloaded as map.osm
% to a MATLAB structure containing part of the information describing the
% transportation network
[parsed_osm, ~] = parse_openstreetmap(fileName);

save('map.mat','parsed_osm');

end

