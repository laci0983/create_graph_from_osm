function geoCodeString = readGeoCodeFromUser()
%This functions returns the destination descriptive string
%received from the user via udp in port 25002.
%@param:  -
%@return: 
%           geoCodeString  - string of the received destination description

udpr=udpport('LocalPort',25002);
flush(udpr);
%setup timer
tic
%read location info
geoCodeString=[];
while isempty(geoCodeString) & toc < 5
    chars=read(udpr,100,"uint8");
    geoCodeString=regexprep(char(chars),'[^a-zA-Z\s]','');
end

if(isempty(geoCodeString))
    geoCodeString=[];
else
    clear udpr
    
end
end

