function [key, val] = get_way_tag_key(tag)
% get tags and key values for ways

key={};
val={};
if(isempty(tag))
    key='';
    val='';
    
else
    if isstruct(tag) == 1
        key = tag.Attributes.k;
        val = tag.Attributes.v;
    elseif iscell(tag) == 1
        for i=1:size(tag,2)
            key{end+1}=tag{i}.Attributes.k;
            val{end+1}=tag{i}.Attributes.v;
        end
    else
        key='';
        val='';
    end
end
end