function [maxDistance] = getMaxDesiredDistance()
%This function reads the maximal desired distance via udp at port 25001
%@param: - 
%@return:
%         maxDistance  - scalar describing the maximal desired distance in
%                        meters
warning('off')

udpr=udpport('LocalPort',25001);
flush(udpr);
%setup timer
tic
%read location info
maxDistance=[];
while isempty(maxDistance) & toc < 5
      maxDistance=read(udpr,1,"single");
end

if(isempty(maxDistance))
    maxDistance=NaN;
end
    
clear udpr
end

