function [connectivity_matrix, intersection_node_indices] = ...
    extract_connectivity2(parsed_osm,node_xys)
%EXTRACT_CONNECTIVITY   extract road connectivity from parsed OpenStreetMap
%   [connectivity_matrix, intersection_nodes] = EXTRACT_CONNECTIVITY(parsed_osm)
%   extracts the connectivity of the road network of the OpenStreetMap
%   file. This yields a set of nodes where the roads intersect.
%
%   Some intersections may appear multiple times, because different roads
%   may meet at the same intersection and because multiple directions are
%   considered different roads. For this reason, in addition to the
%   connectivity matrix, the unique nodes are also identified.
%
% usage
%   [connectivity_matrix, intersection_nodes] = ...
%                                   EXTRACT_CONNECTIVITY(parsed_osm)
%
% input
%   parsed_osm = parsed OpenStreetMap (.osm) XML file,
%                as returned by function parse_openstreetmap
%
% output
%   connectivity_matrix = adjacency matrix of the directed graph
%                         of the transportation network
%                       = adj(i, j) = 1 (if a road leads from node i to j)
%                                   | 0 (otherwise)
%   intersection_nodes = the unique nodes of the intersections
%
% 2010.11.20 (c) Ioannis Filippidis, jfilippidis@gmail.com
%
% See also PARSE_OPENSTREETMAP, PLOT_WAY.
[bounds, node, way, ~] = assign_from_parsed(parsed_osm);
road_vals = {'motorway', 'motorway_link', 'trunk', 'trunk_link',...
    'primary', 'primary_link', 'secondary', 'secondary_link',...
    'tertiary', 'road', 'residential', 'living_street',...
    'service', 'services', 'motorway_junction',...
    'unclassified'};

bridge_vals = {'yes','cantilever','covered','low_water_crossing',...
          'movable','trestle','viaduct'};

road_keys={'highway','bridge'};


numOfWays=size(way.id,2);
nodesAlongWays=way.nd;
nodes=node.id;

for i=1:numOfWays
    % Process only highways
    [key, val] = get_way_tag_key(way.tag{1, i} );
    [key,val,oneWay]=is_one_way(key,val);

    if sum(ismember(road_keys, key) == 1) == 0
        continue;
    end
    
    % Are vehicles allowed in this type of road?
    if sum(ismember(road_vals, val) == 1) == 0 & ...
           sum(ismember(bridge_vals, val) == 1) == 0
        continue; % skip if it's a non drivable road
    end
    
    nodesAlongWay_i=nodesAlongWays{i}; % get current driveable way
    numOfNodesAlongWay_i=size(nodesAlongWay_i,2); %get all nodes of way
    
    for j=1:numOfNodesAlongWay_i-1 %connect each node to its neighbour
        node_j=nodesAlongWay_i(j);
        node_j_index = find(node_j == nodes);
        cords_j=node_xys(:,node_j_index)';
        node_k=nodesAlongWay_i(j+1);
        node_k_index = find(node_k == nodes);
        cords_k=node_xys(:,node_k_index)';

        dist=distance(cords_j(2),cords_j(1),...
                                            cords_k(2),cords_k(1),6371000);
        connectivity_matrix(node_j_index,node_k_index)=dist;
        if(oneWay==0)
            connectivity_matrix(node_k_index,node_j_index)=dist;
          
        end
    end
    
end


% connectivity matrix should not contain any self-loops - diagonal :=0
[row,col]=size(connectivity_matrix);
connectivity_matrix(row,row)=0;
connectivity_matrix(col,col)=0;
connectivity_matrix=connectivity_matrix-diag(diag(connectivity_matrix));


%% unique nodes
nnzrows = any(connectivity_matrix, 2);
nnzcmns = any(connectivity_matrix, 1);
nnznds = nnzrows.' | nnzcmns;
intersection_node_indices = find(nnznds == 1);
% figure;
% spy(connectivity_matrix)
