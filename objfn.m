function objvals=objfn(pop)
%Funciton assigns objective funtion values to all individual,
%Objective funtion is the path needed for the traversal of the graph
%@param:
%       pop     -  population matrix size Nind x Nvar
%@output:
%       objval  -  objective funtion values for each individual (vector)
%                   size: 1xNind

global startPoint;
global toggle;
global repetable;

%For Graphs
global G;

global trendOfError;

Nind=size(pop,1);
objvals=zeros(Nind,1); %All objvals are initialized as infite

%For Graphs
%d = distances(G);
graphEdges=G.Edges.EndNodes;
for i=1:Nind
    edgesToReachInSequence=pop(i,:)';
    startNodes=graphEdges(edgesToReachInSequence,1)';
    endNodes=graphEdges(edgesToReachInSequence,2)';
    
    % create node to reach in sequence
    % in steps
    N = max(numel(startNodes),numel(endNodes));
    ia = 1+rem(0:N-1, numel(startNodes)) ;
    ib = 1+rem(0:N-1, numel(endNodes));
    C = [startNodes(ia) ; endNodes(ib)];
    C = reshape(C,1,[]) ;
    % in a one-liner
    nodesToReachInSequence = reshape([startNodes(1+rem(0:max(numel(startNodes),numel(endNodes))-1, numel(startNodes))) ;...
        endNodes(1+rem(0:max(numel(startNodes),numel(endNodes))-1, numel(endNodes)))],1,[]);
    % ressort nodes to reach in sequence
    for j=1:4:size(nodesToReachInSequence,2)-3
        edge1start=nodesToReachInSequence(j);
        edge1end=nodesToReachInSequence(j+1);

        edge2start=nodesToReachInSequence(j+2);
        edge2end=nodesToReachInSequence(j+3);
        
        [~,dist1Between2Edges,~]=shortestpath(G,edge1end,edge2start);
        [~,dist2Between2Edges,~]=shortestpath(G,edge1end,edge2end);
        if(dist2Between2Edges<dist1Between2Edges) % switch direction if it is shorter
            nodesToReachInSequence(j+2)=edge2end;
            nodesToReachInSequence(j+3)=edge2start;
        end
        
    end
    %    visitedEdges=zeros(size(size(edgesToReachInSequence,1)),1);
    startNode=startPoint;
    for j=1:size(nodesToReachInSequence,2)
        endNode=nodesToReachInSequence(j);
        [nodePath,sectionLength,~]=shortestpath(G,startNode,endNode);
       
        objvals(i)= objvals(i)+sectionLength;
        if(sectionLength<inf)
             startNode=nodePath(end);
        end

        
    end
    if(repetable)
        [~,sectionLength,~]=shortestpath(G,nodesToReachInSequence(end),nodesToReachInSequence(1));
        objvals(i)= objvals(i)+sectionLength;
    end
end


minimalDist=min(objvals);
if(toggle==1)
    trendOfError=[trendOfError; minimalDist];
    toggle=0;
else
    toggle=1;
end

end