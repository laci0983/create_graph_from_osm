function [key,val,oneWay]=is_one_way(key,val)
oneWay=0;
if(~isempty(key))
    Index = find(contains(key,'oneway') | contains(key,'junction'));
    
    if(~isempty(Index))
        if(strcmp(val(Index),'yes')==1 | ...
                                    strcmp(val(Index),'roundabout')==1)
            oneWay=1;
        end
        
    end
    
    if(iscell(key))
        key=key{1};
        val=val{1};
    end
end


end